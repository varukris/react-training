import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import GridListTile from '@material-ui/core/GridListTile';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

const styles = theme => ({
	root: {
		display: 'flex',
		flexWrap: 'wrap',
		justifyContent: 'space-around',
		overflow: 'hidden',
		backgroundColor: theme.palette.background.paper
	},
	paper: {
		padding: theme.spacing.unit * 2,
		textAlign: 'center',
		color: theme.palette.text.secondary
	},
	icon: {
		color: 'rgba(255, 255, 255, 0.54)'
	}
});

class NewsList extends Component {
	render() {
		const { classes, news } = this.props;
		return (
			<GridList cellHeight={180} className={classes.gridList}>
				{news && news.map(item => (
					<Fragment>
						<GridListTile key={item.img}>
							<img className="tile-img" src={item.urlToImage} alt="No Images Available" />
							<GridListTileBar
								title={item.title}
								subtitle={<span>by: {item.author}</span>}
								actionIcon={
									<IconButton className={classes.icon}>
										<InfoIcon />
									</IconButton>
								}
							/>
						</GridListTile>
					</Fragment>
				))}
			</GridList>
		);
	}
}

NewsList.propTypes = {
	news: PropTypes.array.isRequired,
	classes: PropTypes.object.isRequired
};

const newsListcmp = withStyles(styles)(NewsList);
export { newsListcmp as NewsListTestable };

// We don't want to return the plain UserList (component) anymore, we want to return the smart Container
//      > UserList is now aware of state and actions
const composedContainer = withStyles(styles)(NewsList);
export { composedContainer as NewsList };
