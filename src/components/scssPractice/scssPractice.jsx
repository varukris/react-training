import React from 'react';
import './scssPractice.scss';

const scssPractice = () => (
  <div class="practice-container">
    <h1 class="header">Learn Sass</h1>
    <div class="post">
      <h2>Some random title</h2>
      <p class="custom-para">This is a paragraph with some random text in it</p>
    </div>
    <div class="post">
      <h2>Header #2</h2>
      <p>Here is some more random text.</p>
    </div>
    <div class="post">
      <h2>Here is another header</h2>
      <p>Even more random text within a paragraph</p>
    </div>
    <div class="main-post">
      <p>This is the main post. It should extend the class ".common-post" and have its own CSS styles.</p>
    </div>

    <div class="common-post">
      <p>This is a common post and we will extend the styles applied on this paragraph.</p>
    </div>
  </div>
);

export default scssPractice;

