
# Follow along and let's learn scss

## What are we going to do?

Complete step by step exercises to implement scss and learn best practices.

## Exercise 1

```
1. Create a variable $text-color and set it to white. 
2. Create a variable $bg-color and set it to blue. 
3. Then change the value of the background-color property for the .main-container to the $bg-color variable.
4. Then change the value of the color property for the .post and h2 to the $text-color variable.


```

## Exercise 2

nesting can help organize your code by placing child style rules within the respective parent elements:

```
nest the .post in .main-container and then .custom-para in the nested .post

```
## Exercise 3

mixins

```
1. Write a mixin for border-radius and give it a $radius parameter. 
2. It should use all the vendor prefixes from the example. 
3. Then use the border-radius mixin to give the .main-container element a border radius of 10px.

```

## Exercise 3

use of @if, @else if and @else in mixin

```
1. Write a mixin to change color of the custom-para based on the values passed. 
2. Pass mixin a value and use if and else if to compare the parameter passed to the required condition.

for danger- red, for alert - yellow, for success-green

```

## Exercise 4

Use of partials to split the css code. 

Names for partials start with the underscore (_) character, 
which tells Sass it is a small segment of CSS and not to convert it into a CSS file. 
Also, Sass files end with the .scss file extension. 
To bring the code in the partial into another Sass file, use the 'import' directive.

```
1. create a _mixin.scss and cut the mixin written in scssPractice file and paste it there.
2. Then write an @import statement to import a partial named _mixin.scss into the scssPractice.scss file.

```

## Exercise 5

Undestanding the feature of extend.

Extend allows to borrow the css rules already written and instead of copy-pasting same piece of code again.
The extend directive is a simple way to reuse the rules written for one element, then add more for another.

```
Make a class .main-post that extends .common-post and also has a font-size 30px.

```