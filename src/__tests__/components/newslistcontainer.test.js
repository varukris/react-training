import React from 'react';
import { shallow, mount } from 'enzyme';
import thunk from 'redux-thunk';
import * as actions from '../../actions';
import toJson from 'enzyme-to-json';
import configureStore from 'redux-mock-store';
import {NewsContainerTestable} from '../../containers/news-container';
import { news } from '../setup/news';

const middlewares = [thunk]
const mockStore = configureStore(middlewares);
const initialState = {
	isLoading: false,
	data: {
		news: []
	}
};

let store = mockStore(initialState);


describe('<NewsContainer />', () => {
	describe('render()', () => {
	  test('renders the component', () => {
		const dispatch = jest.fn();
		const wrapper = mount(<NewsContainerTestable  dispatch={dispatch} news={initialState.data.news}/>);
		const listLength = wrapper.find('li').length;
		expect(listLength).toEqual(0);
	  });
	});

	describe('render()', () => {
		const dispatch = jest.fn();
		test('renders the component with list', () => {
		  const wrapper =  mount((
			<NewsContainerTestable dispatch={dispatch} news={news.articles}/>
		  ));
		  const listLength = wrapper.find('li').length;
		  expect(listLength).toEqual(20);
		});
	  });
});
