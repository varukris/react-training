export const news = {
	"status":"ok",
	"totalResults":20,
	"articles":[
	   {
		  "source":{
			 "id":null,
			 "name":"Indiatoday.in"
		  },
		  "author":null,
		  "title":"Waterlogged ICU under cleaning process after Patna hospital overflows",
		  "description":"The flooding of a hospital in the capital city of Bihar and fish swimming in floodwater inside the ICU speak volumes about the facilities offered by the government.",
		  "url":"https://www.indiatoday.in/india/story/waterlogged-icu-under-cleaning-process-after-patna-hospital-overflows-1299939-2018-07-30",
		  "urlToImage":"https://akm-img-a-in.tosshub.com/indiatoday/images/story/201807/DjU_bsEUwAIBF0R.jpeg?VAF0w9PcZeEfano18zP4NwBuyYMEJm8m",
		  "publishedAt":"2018-07-30T06:51:48Z"
	   },
	   {
		  "source":{
			 "id":"the-hindu",
			 "name":"The Hindu"
		  },
		  "author":"Special Correspondent",
		  "title":"Sensex, Nifty in the red after touching new highs",
		  "description":"The 30-share Sensex touched a new intra-day high of 37,496.80 during the morning session",
		  "url":"https://www.thehindu.com/business/markets/sensex-nifty-in-the-red-after-touching-new-highs/article24551130.ece",
		  "urlToImage":"https://www.thehindu.com/business/markets/v040wh/article24377304.ece/ALTERNATES/LANDSCAPE_615/sensex",
		  "publishedAt":"2018-07-30T06:25:29Z"
	   },
	   {
		  "source":{
			 "id":"the-hindu",
			 "name":"The Hindu"
		  },
		  "author":"",
		  "title":"Parliament updates live | Rajya Sabha adjourned over NRC draft",
		  "description":"After a three-day break, both Houses of the Parliament are reconvening on Monday. The Lok Sabha will continue its discussion on The Homoeopathy Central Council (Amendment) Bill, 2018, which was introd",
		  "url":"https://www.thehindu.com/news/national/parliament-updates-live-day-8/article24550741.ece",
		  "urlToImage":"https://www.thehindu.com/news/national/n755c4/article24550803.ece/ALTERNATES/LANDSCAPE_615/rajya%20sabhajpg",
		  "publishedAt":"2018-07-30T06:15:54Z"
	   },
	   {
		  "source":{
			 "id":null,
			 "name":"Firstpost.com"
		  },
		  "author":null,
		  "title":"Karunanidhi health updates: Jaggi Vasudev, MDMK's Vaiko call on ailing DMK chief, meet Stalin, Kanimozhi",
		  "description":"DMK supremo Karunanidhi is fighting with Yaman (the God of death) but he will return victorious, said MDMK general secretary Vaiko Kaveri after visting him at Kauvery Hospital on Monday Watch LIVE News, Latest Updates, Live blog, Highlights and Live coverage …",
		  "url":"https://www.firstpost.com/india/karunanidhi-health-live-updates-jaggi-vasudev-mdmks-vaiko-call-on-ailing-dmk-chief-meet-stalin-kanimozhi-4847891.html",
		  "urlToImage":"https://images.firstpost.com/wp-content/uploads/2017/12/Karunanidhi_Reuters.jpg",
		  "publishedAt":"2018-07-30T05:47:23Z"
	   },
	   {
		  "source":{
			 "id":"the-times-of-india",
			 "name":"The Times of India"
		  },
		  "author":"ET Online and Agencies",
		  "title":"Final NRC list released with 2.9 crore citizens. Here is how you can check your name",
		  "description":"",
		  "url":"https://economictimes.indiatimes.com/news/politics-and-nation/nrc-assam-on-high-alert-cm-calls-for-calm-ahead-of-historic-moment/articleshow/65191859.cms",
		  "urlToImage":"https://img.etimg.com/thumb/msid-65191857,width-1070,height-580,imgsize-324506,overlay-economictimes/photo.jpg",
		  "publishedAt":"2018-07-30T05:45:42Z"
	   },
	   {
		  "source":{
			 "id":null,
			 "name":"India.com"
		  },
		  "author":"Zee Media Bureau",
		  "title":"7 members of a family commit suicide in Ranchi",
		  "description":"Sunny Sharad",
		  "url":"http://zeenews.india.com/india/7-members-of-a-family-commit-suicide-in-ranchi-2128638.html",
		  "urlToImage":"http://ste.india.com/sites/default/files/2018/07/30/707842-suicide-zee.jpg",
		  "publishedAt":"2018-07-30T05:39:00Z"
	   },
	   {
		  "source":{
			 "id":null,
			 "name":"Firstpost.com"
		  },
		  "author":null,
		  "title":"Maratha quota stir: Devendra Fadnavis announces partial withdrawal of cases filed against youth protesters",
		  "description":"Maharashtra chief minister Devendra Fadnavis announced that there will be partial withdrawal of the cases filed against the Maratha quota youth protesters",
		  "url":"https://www.firstpost.com/india/maratha-quota-stir-devendra-fadnavis-announces-partial-withdrawal-of-cases-filed-against-youth-protesters-4848811.html",
		  "urlToImage":"https://images.firstpost.com/wp-content/uploads/2018/07/Devendra-Fadnavis-pti.jpg",
		  "publishedAt":"2018-07-30T05:26:33Z"
	   },
	   {
		  "source":{
			 "id":null,
			 "name":"News18.com"
		  },
		  "author":"Contributor Content",
		  "title":"Shravan Maas Shiva Puja 2018 – The Legend, Rituals & Shivratri",
		  "description":"The Shravan Maas will be observed in North India from 28th July to 26th August 2018, while in South India, it will begin on 12th August and will conclude on 9th September 2018.",
		  "url":"https://www.news18.com/news/india/shravan-maas-shiva-puja-2018-the-legend-rituals-shivratri-1827721.html",
		  "urlToImage":"https://images.news18.com/ibnlive/uploads/2018/02/Happy-Shivratri.jpg",
		  "publishedAt":"2018-07-30T05:09:00Z"
	   },
	   {
		  "source":{
			 "id":null,
			 "name":"Financialexpress.com"
		  },
		  "author":"Devanjana Nag",
		  "title":"Old Yamuna bridge in Delhi closed! Several Indian Railways trains cancelled and diverted; full list here",
		  "description":"The old Yamuna bridge in the capital city, which is also known as 'Loha Pul' has been temporarily closed for rail traffic as the water level of the river has reached the 205.53 mark, according to Indian Railways.",
		  "url":"https://www.financialexpress.com/infrastructure/railways/old-yamuna-bridge-in-delhi-closed-several-indian-railways-trains-cancelled-and-diverted-full-list-here/1262823/",
		  "urlToImage":"https://images.financialexpress.com/2018/07/loha-pul-660.jpg",
		  "publishedAt":"2018-07-30T05:06:04Z"
	   },
	   {
		  "source":{
			 "id":null,
			 "name":"Ndtv.com"
		  },
		  "author":"NDTVSports.com",
		  "title":"Chris Gayle Equals Shahid Afridi's Record For Most Sixes In International Cricket",
		  "description":"Both the batsmen now hold 476 sixes to their name.",
		  "url":"https://sports.ndtv.com/cricket/chris-gayle-equals-shahid-afridis-record-for-most-sixes-in-international-cricket-1891859",
		  "urlToImage":"https://c.ndtvimg.com/flns5g48_chris-gayle-icc_625x300_30_July_18.jpg",
		  "publishedAt":"2018-07-30T05:01:48Z"
	   },
	   {
		  "source":{
			 "id":null,
			 "name":"Indianexpress.com"
		  },
		  "author":"Express Web Desk",
		  "title":"Katrina Kaif replaces Priyanka Chopra in Salman Khan's Bharat",
		  "description":"Katrina Kaif will be joining the shoot in the upcoming schedule beginning in September. Bharat began production earlier this month and the first look of the Salman Khan film is already out.",
		  "url":"https://indianexpress.com/article/entertainment/bollywood/katrina-kaif-replaces-priyanka-chopra-salman-khan-bharat-5282418/",
		  "urlToImage":"https://images.indianexpress.com/2018/07/katrina-kaif-7598.jpg?w=759",
		  "publishedAt":"2018-07-30T04:45:27Z"
	   },
	   {
		  "source":{
			 "id":null,
			 "name":"Livemint.com"
		  },
		  "author":"Parizad Sirwalla",
		  "title":"Use ITR-3 for returns, if you have both income from profession and capital gain",
		  "description":"Since you have both income from specified profession (as per Section 44ADA read with Section 44AA of the Income Tax Act, 1961) and capital gain, you will need to use  ITR-3 for filing your tax return for the financial year 2017-18",
		  "url":"https://www.livemint.com/Money/GNlgCsIZfYs6l1ODBOsPAO/Use-ITR3-for-returns-if-you-have-both-income-from-professi.html",
		  "urlToImage":"https://www.livemint.com/rf/Image-621x414/LiveMint/Period2/2018/07/30/Photos/Processed/itr-k4ND--621x414@LiveMint.jpg",
		  "publishedAt":"2018-07-30T04:26:44Z"
	   },
	   {
		  "source":{
			 "id":"the-times-of-india",
			 "name":"The Times of India"
		  },
		  "author":"Subhash Mishra",
		  "title":"I'm not scared of meeting industrialists in public: PM Modi",
		  "description":"India News: PM Narendra Modi on Sunday reached out to the business community, looking to reassure industrialists that they have a significant role to play in nati",
		  "url":"https://timesofindia.indiatimes.com/india/im-not-scared-of-meeting-industrialists-in-public-pm/articleshow/65190708.cms",
		  "urlToImage":"https://static.toiimg.com/thumb/msid-65191531,width-1070,height-580,imgsize-185133,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg",
		  "publishedAt":"2018-07-30T02:15:29Z"
	   },
	   {
		  "source":{
			 "id":"the-times-of-india",
			 "name":"The Times of India"
		  },
		  "author":null,
		  "title":"J&K terrorists barge into CRPF jawan's home, kill him",
		  "description":"India News: A CRPF jawan was shot dead by terrorists at his home in Jammu & Kashmir's Pulwama district on Sunday.",
		  "url":"https://timesofindia.indiatimes.com/india/jk-terrorists-barge-into-crpf-jawans-home-kill-him/articleshow/65190947.cms",
		  "urlToImage":"https://static.toiimg.com/thumb/msid-65190965,width-1070,height-580,imgsize-156213,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg",
		  "publishedAt":"2018-07-30T00:13:30Z"
	   },
	   {
		  "source":{
			 "id":"the-times-of-india",
			 "name":"The Times of India"
		  },
		  "author":"Bagish Jha",
		  "title":"10 of 15 nightclubs on Gurugram's Mall Mile face closure for 'sex trade'",
		  "description":"GURUGRAM: MG Road’s Mall Mile, the first major nightlife hotspot to come up in Delhi-NCR, is in big trouble.",
		  "url":"https://timesofindia.indiatimes.com/city/gurgaon/10-of-15-nightclubs-on-gurugrams-mall-mile-face-closure-for-sex-trade/articleshow/65190721.cms",
		  "urlToImage":"https://static.toiimg.com/thumb/msid-65190724,width-1070,height-580,imgsize-1393547,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg",
		  "publishedAt":"2018-07-29T21:22:22Z"
	   },
	   {
		  "source":{
			 "id":"the-times-of-india",
			 "name":"The Times of India"
		  },
		  "author":"Rachel Chitra",
		  "title":"Hackers deposit Re 1 in Trai chief's account",
		  "description":"India News:  Ethical hackers on Sunday claimed to have the bank account details of Trai chairman R S Sharma, after he had posted his Aadhaar number online. Twitte",
		  "url":"https://timesofindia.indiatimes.com/india/hackers-deposit-re-1-in-trai-chiefs-account/articleshow/65190556.cms",
		  "urlToImage":"https://static.toiimg.com/thumb/msid-65190567,width-1070,height-580,imgsize-130603,resizemode-6,overlay-toi_sw,pt-32,y_pad-40/photo.jpg",
		  "publishedAt":"2018-07-29T19:51:28Z"
	   },
	   {
		  "source":{
			 "id":null,
			 "name":"Dailypioneer.com"
		  },
		  "author":"The Pioneer",
		  "title":"Priest threatens witness to withdraw case",
		  "description":"In yet another alleged bid to sabotage the rape case against Jalandhar&rsquo;s Latin Catholic Bishop Franco Mulakkal who is accused of sexually abusing a senior nun, a priest belonging to the",
		  "url":"https://www.dailypioneer.com/nation/priest-threatens-witness-to-withdraw-case.html",
		  "urlToImage":"https://www.dailypioneer.com/images-tdp/logo.jpg",
		  "publishedAt":"2018-07-29T18:53:18Z"
	   },
	   {
		  "source":{
			 "id":null,
			 "name":"Dailyexcelsior.com"
		  },
		  "author":null,
		  "title":"Trump admin set to announce major Indo-Pacific economic, developmental initiatives",
		  "description":null,
		  "url":"http://www.dailyexcelsior.com/trump-admin-set-to-announce-major-indo-pacific-economic-developmental-initiatives/",
		  "urlToImage":null,
		  "publishedAt":"2018-07-29T18:46:34Z"
	   },
	   {
		  "source":{
			 "id":null,
			 "name":"Indianexpress.com"
		  },
		  "author":"Education Desk",
		  "title":"ICAI CA IPCC Intermediate Result 2018: Results declared at icai.nic.in",
		  "description":"ICAI CA IPCC Intermediate Result May 2018:  All the candidates who have appeared for the examination can check the results through the official websites, icaiexam.icai.org, caresults.icai.org and icai.nic.in",
		  "url":"https://indianexpress.com/article/education/icai-ca-intermediate-result-today-at-icai-nic-in-how-to-check-5281472/",
		  "urlToImage":"https://images.indianexpress.com/2018/07/icai.jpg?w=759",
		  "publishedAt":"2018-07-29T15:07:57Z"
	   },
	   {
		  "source":{
			 "id":null,
			 "name":"Indianexpress.com"
		  },
		  "author":"Girish Kuber",
		  "title":"So where is the Mahagathbandhan that will challenge the BJP? asks Amit Shah",
		  "description":"BJP president Amit Shah speaks to The Indian Express on the Opposition challenge to the BJP, the party’s own difficulties \r\nwith allies, the criticism over lynchings and cow vigilantism, inner-party democracy and demands for reservation.",
		  "url":"https://indianexpress.com/article/india/amit-shah-bjp-alliances-mahagathbandhan-2019-general-election-5281048/",
		  "urlToImage":"https://images.indianexpress.com/2018/07/amit-shah-1.jpg?w=759",
		  "publishedAt":"2018-07-28T19:29:50Z"
	   }
	]
 };
