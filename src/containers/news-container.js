import React, { Component } from 'react';
import { bindActionCreators, compose } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import { NewsList } from '../components/NewsList';
import { getNews } from '../actions/index';

const styles = theme => ({
	root: {
		display: 'flex',
		flexWrap: 'wrap',
		justifyContent: 'space-around',
		overflow: 'hidden',
		backgroundColor: theme.palette.background.paper
	},
	paper: {
		padding: theme.spacing.unit * 2,
		textAlign: 'center',
		color: theme.palette.text.secondary
	},
	icon: {
		color: 'rgba(255, 255, 255, 0.54)'
	}
});

class NewsContainer extends Component {
	componentDidMount() {
		const { dispatch } = this.props;
		dispatch(getNews());
	}

	generate(array = [], element) {
		console.log(array);
		return array.map(value =>
			React.cloneElement(element, {
				key: value
			}));
	}

	render() {
		console.log('rendered');
		const { classes, news } = this.props;
		return (
			<div className={classes.root}>
				<div className="border-list">
					<NewsList news={news} />
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {
	console.log(state);
	return {
		news: state.news.data.news
	};
}

function mapDispatchToProps(dispatch) {
	const actions = bindActionCreators({ getNews });
	return { ...actions, dispatch };
}

NewsContainer.propTypes = {
	dispatch: PropTypes.func.isRequired,
	classes: PropTypes.object.isRequired,
	news: PropTypes.array
};

const testExport = withStyles(styles)(NewsContainer);

export { testExport as NewsContainerTestable };

// We don't want to return the plain UserList (component) anymore, we want to return the smart Container
//      > UserList is now aware of state and actions
const composedContainer = withStyles(styles)(
	compose(
		connect(
			mapStateToProps,
			mapDispatchToProps
		)
	)(NewsContainer)
);
export { composedContainer as NewsContainer };
