import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { getStoreInstance } from './store';
import './index.css';
import App from './App';

import registerServiceWorker from './registerServiceWorker';

const store = getStoreInstance();
ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
);
registerServiceWorker();
