// Packages
import queryString from 'query-string';
export default class ApiUtils {
	// HTTP Methods
	static METHOD_GET = 'GET';

	static METHOD_POST = 'POST';

	static METHOD_PUT = 'PUT';

	static METHOD_DELETE = 'DELETE';

	// Response Status
	static RESPONSE_SUCCESS = 'ok';

	static RESPONSE_FAIL = 'fail';

	static RESPONSE_ERROR = 'error';

	static RESPONSE_REVIEW = 'review';

	static handleErrors(resp) {
		console.log(resp);
		switch (resp.status) {
			case 401:
				alert('Un authorized');
				break;

			case 400:
			case 404:
			case 500:
			case 504:
				throw new Error('Error Occured');

			default:
				break;
		}
	}

	/**
	 * Common utility function to send ajax requests
	 *
	 * @method     makeAjaxRequest
	 * @param      {object}    options
	 * @param      {Function}  onSuccess
	 * @param      {Function}  onFail
	 */

	static async makeAjaxRequest(options, onSuccess, onFail) {
		const mimeType = 'application/json';
		const request = new Request(options.url, {
			method: 'GET',
			body: JSON.stringify(options.payload)
		});

		try {
			let parsedResponse = {};
			const resp = await fetch(request);
			this.handleErrors(resp);
			if (resp.status === 200) {
				if (options.type === 'html') {
					parsedResponse = await resp.text();
				} else {
					parsedResponse = await resp.json();
				}
			}

			if (typeof onSuccess === 'function') {
				const modifiedResponse = onSuccess(parsedResponse);

				if (typeof modifiedResponse !== 'undefined') {
					// If callback fn modifies and RETURN some res,
					// then pass the new res to next .then()
					parsedResponse = modifiedResponse;
				}
			}

			return parsedResponse;
		} catch (err) {
			if (typeof onFail === 'function') {
				onFail(err);
			}
			return err;
		}
	}
}
