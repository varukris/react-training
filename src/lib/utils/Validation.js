export const required = errorMsg => value => (value ? undefined : locale.required(errorMsg));

export const maxLength = max => value => {
	return (value && value.length) > max ? locale.maxLength(max) : undefined;
};

export const email = errMsg => value => {
	return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? errMsg : undefined;
};

export const isValidName = errMsg => value => {
	return value && /[(!@#$%)]/i.test(value) ? errMsg : undefined;
};

/* Password validation to check if contains
   One uppercase, one lowercase, one number, one special character , and no spaces
*/
export const isValidPassword = errMsg => value => {
	return value && !/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?!.*\s)(?=.{6,})/i.test(value)
		? errMsg
		: undefined;
};
