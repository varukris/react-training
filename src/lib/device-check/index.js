export default function isMobile() {
	return window.innerWidth < 1024;
}
