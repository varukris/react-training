/* App React Component
** Renders the Main Application
*/

// Packages
import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';

// Utils
import { history } from './store';

// Components
import { NewsContainer } from './containers/news-container';

// Styles
import './App.scss';

class App extends Component {
	render() {
		return (
			<ConnectedRouter history={history}>
				<Switch>
					<Route path="/" exact component={NewsContainer} />
				</Switch>
			</ConnectedRouter>
		);
	}
}

export default App;
