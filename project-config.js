/*
* Project Setting & Configuration
* Compilation :  ReactJS, SCSS
* Build Tool : Webpack v3
*/

// Application
const APP_NAME = 'Studio News App';
const APP_TITLE = 'Studio App';
const VERSION = 'v1-0-0';

// Source Code
const SOURCE_ROOT_FOLDER = 'src';

// Common JS Library/Code
const JS_LIB = `${SOURCE_ROOT_FOLDER}/lib`;

// Style Bundling
const STYLESHEETS = `${SOURCE_ROOT_FOLDER}/stylesheets`;

// Assets for Copying to Server
const SOURCE_ASSETS_FOLDER = `${SOURCE_ROOT_FOLDER}/assets`; // [Fonts, Images, Videos, Docs etc]

/*
*    /site-name  => If site is hosted on Shared Machine
*    /           => If site is hosted on Dedicated Machine
*/
const APP_PUBLIC_PATH = '';

// Webpack-Dev-Server
const DEV_SERVER_HOST = 'localhost'; // Use 0.0.0.0 if wanted to access it over LAN using Machine IP address
const DEV_SERVER_PORT = 3000;

// API Server
// const API_BASE_URL = 'http://<HOST_NAME>:<PORT>';
